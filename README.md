# Ghost Backup

Shell script that backs up ghost volume to S3, should be run on cron.

Modified from:
https://jmrobles.medium.com/how-backup-your-postgres-db-into-aws-s3-in-kubernetes-edf6cf0db11

```
# Daily at 2am: "0 2 * * *"
# Every 5 min: "*/5 * * * *"
```

## Sample k8 yaml

```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: ghost-backup-cron
  namespace: ghost-NAME
spec:
  concurrencyPolicy: Forbid
  successfulJobsHistoryLimit: 1
  failedJobsHistoryLimit: 3
  schedule: "0 5 * * *"
  jobTemplate:
    spec:
      backoffLimit: 3
      parallelism: 1
      template:
        spec:
          restartPolicy: OnFailure
          containers:
            - name: ghost-backup
              image: registry.gitlab.com/theplacelab/image/ghost-backup
              volumeMounts:
                - name: content
                  mountPath: /ghost
              env:
                - name: BACKUP_NAME
                  value: "ghost-NAME"
                - name: BACKUP_PASSWORD
                  value: XXX
                - name: S3_ACCESS_KEY_ID
                  value: XXX
                - name: S3_SECRET_ACCESS_KEY
                  value: XXX
                - name: S3_PATH
                  value: XXX
                - name: S3_REGION
                  value: "us-east-1"
                - name: S3_ENDPOINT
                  value: "https://s3.wasabisys.com"
          volumes:
            - name: content
              persistentVolumeClaim:
                claimName: NAME-content
```

## Sample docker-compose.yml

```
ghost_backup:
    image: registry.gitlab.com/theplacelab/image/ghost-backup
    environment:
      BACKUP_NAME: XXX
      BACKUP_PASSWORD: XXX
      S3_ACCESS_KEY_ID: XXX
      S3_SECRET_ACCESS_KEY: XXX
      S3_PATH: s3://dbbackup.theplacelab.com/
      S3_REGION: us-east-1
      S3_ENDPOINT: https://s3.wasabisys.com
    volumes:
      - ghost_feral_data:/ghost
```

## Getting your backups

```
mcrypt -d FILE.tar.bz2.nc
mv ./ghost ./content
kubectl cp ./content NAMESPACE/PODNAME:/var/lib/ghost/
```
