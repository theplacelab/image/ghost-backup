#!/bin/bash
curl -s -4 https://icanhazip.com/
export BACKUP_FILE="$BACKUP_NAME"_`date +%Y%m%d_%H%M%S`.tar.bz2
tar -cjf $BACKUP_FILE ghost/
mcrypt $BACKUP_FILE -k $BACKUP_PASSWORD
AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY aws s3 cp $BACKUP_FILE.nc $S3_PATH --region=$S3_REGION --endpoint-url=$S3_ENDPOINT 
rm $BACKUP_FILE
rm $BACKUP_FILE.nc